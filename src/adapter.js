/* global shaka */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Shaka = youbora.Adapter.extend({

  constructor: function(player, tag) {
    youbora.adapters.Shaka.__super__.constructor.call(this, player)
    this.tag = tag ? tag : null
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var tag = this.tag || this._getTagFromPlayer()
    return tag ? tag.currentTime : null
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    if (typeof this.player.getPlaybackRate === 'function') {
      return this.player.getPlaybackRate() || 1
    }
    return 1
  },

  /** Override to return Frames Per Secon (FPS) */
  getFramesPerSecond: function () {
    var track = this._getActiveTrack()
    return track ? track.frameRate : null
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.player.getStats().droppedFrames
  },

  /** Override to return video duration */
  getDuration: function () {
    var tag = this.tag || this._getTagFromPlayer()
    return tag ? tag.duration : null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var stats = this.player.getStats()
    var ret = null
    if (typeof stats.streamBandwidth !== 'undefined') {
      ret = stats.streamBandwidth
    }
    if (typeof stats.streamStats !== 'undefined') {
      ret = stats.streamStats.videoBandwidth
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    var track = this._getActiveTrack()
    return track ? youbora.Util.buildRenditionString(track.width, track.height, track.bandwidth) : null
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    return this.player.getStats().estimatedBandwidth
  },

  /** Override to return title */
  getTitle: function () {
    var tag = this.tag || this._getTagFromPlayer()
    return tag ? tag.title : null
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.player.isLive() ? true : !this.getDuration()
  },

  /** Override to return resource URL. */
  getResource: function () {
    var tag = this.tag || this._getTagFromPlayer()
    var ret = tag ? tag.currentSrc : null
    if (this.player.getAssetUri && this.player.getAssetUri()) {
      ret = this.player.getAssetUri()
    } else if (this.player.getManifestUri && this.player.getManifestUri()) {
      ret = this.player.getManifestUri()
    }
    return ret
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var ret = null
    if (typeof shaka !== 'undefined') {
      if (shaka.Player) {
        ret = shaka.Player.version
      } if (shaka.player) {
        ret = shaka.player.Player.version
      }
    }
    return ret
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Shaka'
  },

  /** Get the active track, to get playrate, rendition... */
  _getActiveTrack: function () {
    var ret = null
    if (this.player.getVariantTracks) {
      var tracks = this.player.getVariantTracks()
      for (var i in tracks) {
        var track = tracks[i]
        if (track.active && (track.type === 'video' || track.type === 'variant')) {
          ret = track
        }
      }
    } else if (this.player.getVideoTracks) {
      var tracks2 = this.player.getVideoTracks()
      for (var j in tracks2) {
        var track2 = tracks2[j]
        if (track2.active) {
          ret = track2
        }
      }
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // References
    this.references = {
      play: this.playListener.bind(this),
      loadstart: this.autoplayListener.bind(this),
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      ended: this.endedListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this)
    }

    this.referencesPlayer = {
      error: this.errorListener.bind(this)
    }

    // Register listeners
    this.tag = null
    this._registerTag()
    for (var key2 in this.referencesPlayer) {
      this.player.addEventListener(key2, this.referencesPlayer[key2])
    }
  },

  _registerTag: function () {
    this.tag = this.tag || this._getTagFromPlayer()
    if (this.tag) {
      for (var key in this.references) {
        this.tag.addEventListener(key, this.references[key])
      }
    } else {
      setTimeout(this._registerTag.bind(this), 50)
    }
  },

  _getTagFromPlayer() {
    return this.player.getMediaElement ? this.player.getMediaElement() : this.player.a
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.tag && this.references) {
      for (var key in this.references) {
        this.tag.removeEventListener(key, this.references[key])
      }
      delete this.references
    }

    if (this.player && this.referencesPlayer) {
      for (var key2 in this.referencesPlayer) {
        this.player.removeEventListener(key2, this.referencesPlayer[key2])
      }
      delete this.referencesPlayer
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this._callStart()
  },

  /** Listener for 'play' event. */
  autoplayListener: function (e) {    
    var tag = this.tag || this._getTagFromPlayer()
    if (tag && tag.autoplay) {
      this._callStart()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this._callStart()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    // Error codes: https://shaka-player-demo.appspot.com/docs/api/shaka.util.Error.html
    var code = e.detail && e.detail.code? e.detail.code : e.code;
    var msg = e.detail && e.detail.message ? e.detail.message : 'unknown'
    var category = e.detail && e.detail.category? e.detail.category : e.category
    var severity = e.detail && e.detail.severity? e.detail.severity : e.severity;
    if (category && category < 10 && category > 0) {
      var typeDicc = {
        1: 'network',
        2: 'text',
        3: 'media',
        4: 'manifest',
        5: 'streaming',
        6: 'drm',
        7: 'player',
        8: 'cast',
        9: 'storage'
      }
      msg = typeDicc[category] || msg
    }
    if (severity === 2 && [1002,3016].indexOf(code) === -1) {
      this.fireFatalError(code,msg) // code is not on the list and severity is 2: fatal
    } else {
      this.fireError(code,msg) // not severity 2, or in the list: nonfatal
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
    this.autoplayListener()
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    if (this.flags.isBuffering) {
      this.fireBufferEnd()
    }
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  },

  timeupdateListener: function (e) {
    var tag = this.tag || this._getTagFromPlayer()
    if ((tag && !tag.autoplay) || (this.getPlayhead() !== this.initialPlayhead && typeof this.initialPlayhead === 'number')) {
      this._callStart()
    }
  },

  _callStart: function () {
    if (!this.flags.isStarted) {
      this.fireStart()
      this.initialPlayhead = this.getPlayhead()
    }
    if (this._initialPlayheadChanged() ) this.fireJoin()
  },

  _initialPlayheadChanged: function() {
    var ret = false
    if (!this.flags.isJoined) {
      var current = this.getPlayhead()
      var initial = this.initialPlayhead || 0
      var live = this.plugin ? this.plugin.getIsLive() : this.getIsLive()
      if ( ((initial !== 0 && live) || !live) && current > initial) {
        ret = true
      } else if (live) {
        this.initialPlayhead = current
      }
    }
    return ret
  }
})

module.exports = youbora.adapters.Shaka
